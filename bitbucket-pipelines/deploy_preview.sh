#!/bin/bash

set -ex

./mo stagehand.json.mo >> stagehand.json

curl \
    --header "Content-Type: application/json" \
    --request POST \
    --data @stagehand.json \
    https://stagehand.au.ngrok.io/api/preview
