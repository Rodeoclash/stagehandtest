#! /bin/sh

echo "--- Pulling"
docker pull rancher/hello-world
docker pull crccheck/hello-world

echo "--- Running"
docker-compose up
