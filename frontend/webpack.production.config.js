const webpack = require('webpack')
const config = require('./webpack.config.js')

config.plugins = config.plugins.concat([
  new webpack.optimize.UglifyJsPlugin()
])

module.exports = config
