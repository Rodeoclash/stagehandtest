const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: {
    app: './src/app.js',
  },
  devtool: 'eval',
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, './build'),
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          'babel-loader'
        ],
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
          'postcss-loader',
        ],
      },
      {
        test: /\.cssm$/,
        use: [
          'style-loader',
          'css-loader?modules&importLoaders=1&localIdentName=[path][name]---[local]---[hash:base64:5]',
          'postcss-loader',
        ],
      },
      {
        test: /\.(jpg|png|gif)$/,
        use: [
          'file-loader',
        ]
      },
      {
        test: /\.svg$/,
        use: [
          'babel-loader',
          'react-svg-loader',
        ]
      }
    ]
  },
  devServer: {
    contentBase: path.join(__dirname, 'build'),
    compress: false,
    disableHostCheck: true,
    host: '0.0.0.0',
    port: 8080,
    stats: {
      colors: true,
      errorDetails: true,
      progress: true,
    },
  },
  resolve: {
    alias: {
      cv: path.resolve('./src/cv'),
    },
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: `'${process.env.NODE_ENV || 'development'}'`,
      },
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.ejs',
      title: 'Samuel Richardson CV',
    }),
  ],
}
