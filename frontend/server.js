var express = require('express')
var path = require('path')
var PORT = process.env.PORT || 3000
var IP = process.env.IP || '0.0.0.0'
var dir = path.join(__dirname, 'build')

var app = express()

app.use(express.static(dir))

app.listen(PORT, IP, function () {
  console.log(dir + ' listening on: ' + IP + ':' + PORT)
})
