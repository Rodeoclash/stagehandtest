import StateMachine from 'javascript-state-machine'
import Game         from './cv/Game.js'
import Ui           from './cv/Ui.js'
import './app.css'

const ui = new Ui()
const game = new Game({ui})

const fsm = StateMachine.create({
  events: [
    { name: 'boot', from: 'none', to: 'attract' },
  ],
  callbacks: {
    onattract: function () {
      game.attract()
    },
  },
})

fsm.boot()
