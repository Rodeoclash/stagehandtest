import React from 'react'
import ReactDOM from 'react-dom'
import Root from './Ui/Root.jsx'

export default class Ui {

  constructor() {
    const element = document.createElement('div')
    document.body.appendChild(element)
    this.render = ReactDOM.render(<Root />, element)
  }

  setBaseState(id, active) {
    const activeBase = active ? id : null
    const newState = {
      activeBase,
    }

    this.render.setState(newState)
  }
}
