import { World } from 'ces'
import Matter from 'matter-js'
import $ from 'jquery'
import { times } from 'lodash/fp'
import data from 'cv/data.js'

import Base from './Game/entities/Base.js'
import LevelEntity from './Game/entities/Level.js'
import PlayerEntity from './Game/entities/Player.js'

import BodyManagementSystem from './Game/systems/BodyManagement.js'
import CollisionsSystem from './Game/systems/Collisions.js'
import ControlSystem from './Game/systems/Control.js'
import ForcesSystem from './Game/systems/Forces.js'
import KeyboardEventsSystem from './Game/systems/KeyboardEvents.js'
import RenderSystem from './Game/systems/Render.js'
import CameraSystem from './Game/systems/Camera.js'
import { getSize } from './Game/shared/window.js'

const element = document.createElement('div');
document.body.appendChild(element)

const size = getSize()
const engine = Matter.Engine.create()

engine.world.gravity.y = .33;

const render = Matter.Render.create({
  element,
  engine,
  options: {
    background: '#111',
    hasBounds: true,
    width: size.width,
    height: size.height,
    wireframes: false,
  }
})

class Game {
  constructor({ui}) {
    this.ui = ui
    this.world = new World()

    const renderSystem = new RenderSystem({engine, render})
    const collisionsSystem = new CollisionsSystem({engine, ui})
    const bodyManagementSystem = new BodyManagementSystem({engine})
    const keyboardInputSystem = new KeyboardEventsSystem()
    const controlSystem = new ControlSystem()
    const forcesSystem = new ForcesSystem()
    const cameraSystem = new CameraSystem({engine, render})

    this.world.addSystem(renderSystem)
    this.world.addSystem(bodyManagementSystem)
    this.world.addSystem(collisionsSystem)
    this.world.addSystem(keyboardInputSystem)
    this.world.addSystem(controlSystem)
    this.world.addSystem(forcesSystem)
    this.world.addSystem(cameraSystem)

    $(window).trigger('resize')
    this.boundStep = this.step.bind(this)
    window.requestAnimationFrame(this.boundStep)
  }

  step(timestamp) {
    this.world.update(timestamp)
    window.requestAnimationFrame(this.boundStep)
  }

  attract() {

    const locations = [
      {
        x: 400,
        y: 575,
      },
      {
        x: 130,
        y: 300,
      },
      {
        x: 668,
        y: -20,
      },
      {
        x: 668,
        y: -420,
      },
      {
        x: 171,
        y: -720,
      },
      {
        x: 669,
        y: -920,
      },
      {
        x: 400,
        y: -1330,
      },
      {
        x: 30,
        y: -1510,
      },
      {
        x: 770,
        y: -1710,
      },
      {
        x: 400,
        y: -2110,
      },
    ]

    times((n) => {
      const datum = data[n]
      const location = locations[n]

      this.world.addEntity(Base({
        id: datum.id,
        name: datum.label,
        sprite: require(`./Game/assets/title_${datum.id}.png`),
        x: location.x,
        y: location.y,
      }))
    })(10)

    // Level and player
    this.world.addEntity(PlayerEntity())
    this.world.addEntity(LevelEntity())

  }
}

export default Game
