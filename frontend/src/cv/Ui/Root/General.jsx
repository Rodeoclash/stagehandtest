import React from 'react'

import { flow, map } from 'lodash/fp'

import Link from './General/Link.jsx'
import Panel from 'cv/Ui/shared/Panel.jsx'
import ReactMarkdown from 'react-markdown'

import styles from './General.cssm'

export default function(props) {
  const {
    about,
    label,
    links,
    period,
    technologies,
    role,
  } = props

  const renderedLinks = flow(
    map((link) => {
      return (
        <Link
          {...link}
          key={link.url}
        />
      )
    }),
  )(links)

  const renderedTechnologies = flow(
    map((technology) => {
      return (
        <div
          className={styles.technology}
          key={technology}
        >
          {technology}
        </div>
      )
    })
  )(technologies)

  return (
    <Panel
      from={period[0]}
      subtitle={role}
      title={label}
      to={period[1]}
    >
      <div className={styles.content}>
        <div className={styles.left}>
          <div className={styles.about}>
            <ReactMarkdown source={about} />
          </div>
          <div className={styles.links}>
            {renderedLinks}
          </div>
        </div>
        <div className={styles.right}>
          <div className={styles.technologies}>
            {renderedTechnologies}
          </div>
        </div>
      </div>
    </Panel>
  )
}
