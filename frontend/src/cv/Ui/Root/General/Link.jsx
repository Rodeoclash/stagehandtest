import React from 'react'

import styles from './Link.cssm'

export default function(props) {
  const {
    name,
    url,
  } = props

  return (
    <a
      className={styles.root}
      href={url}
      target='_blank'
    >
      {name}
    </a>
  )
}

