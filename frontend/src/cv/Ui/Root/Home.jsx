import React from 'react'

import Panel from 'cv/Ui/shared/Panel.jsx'
import Arrows from './Home/arrows.svg'

import styles from './Home.cssm'

export default function(props) {
  return (
    <Panel
      title='Welcome'
    >
      <div className={styles.root}>
        <div className={styles.intro}>
          <p>
            This is the interactive CV of Melbourne based full stack developer Sam Richardson.
          </p>
          <p>
            Control the spaceship to fly through the history of my career. Land on each base to read more about my time in that position.
          </p>
          <p>
            P: <a href='tel:+61 405 472748'>+61 405 472 748</a><br/>
            E: <a href='mailto:sam@richardson.co.nz'>sam@richardson.co.nz</a>
          </p>
        </div>
        <div className={styles.controls}>
          <Arrows />
          <p>
            <em>
              Use the arrow keys to control the spaceship.
            </em>
          </p>
        </div>
      </div>
    </Panel>
  )
}
