import React from 'react'

import styles from './Panel.cssm'

export default function Panel(props) {
  const {
    children,
    from,
    subtitle,
    title,
    to,
  } = props

  const renderedTimespan = (
    <span className={styles.timespan}>
      {from} - {to}
    </span>
  )

  return (
    <section className={styles.root}>
      <header>
        <h1>
          {title}
        </h1>
        {from && to ? renderedTimespan : null}
        <div className={styles.subtitle}>
          {subtitle}
        </div>
      </header>
      <div className={styles.body}>
        {children}
      </div>
    </section>
  )
}
