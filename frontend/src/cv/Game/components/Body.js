import { Component } from 'ces'

const Body = Component.extend({
  name: 'body',
  init: function (body) {
    this.body = body
  },
})

export default Body
