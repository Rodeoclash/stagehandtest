import { Component } from 'ces'

const Bodies = Component.extend({
  name: 'bodies',
  init: function (bodies) {
    this.bodies = bodies
  },
})

export default Bodies
