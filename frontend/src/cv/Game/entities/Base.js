import { Entity }             from 'ces'
import {
  Bodies,
  Composite,
}                             from 'matter-js'
import BodyComponent          from '../components/Body.js'

const WIDTH = 200
const HEIGHT = 10

export default function({id, name, sprite, x, y}) {
  const base = new Entity()

  const title = Bodies.rectangle(x, y + HEIGHT * 2, WIDTH / 2, WIDTH / 5, {
    isStatic: true,
    render: {
      sprite: {
        texture: sprite,
      },
    },
  })

  const floor = Bodies.rectangle(x, y, WIDTH, HEIGHT, {
    uiId: id,
    isStatic: true,
    label: 'base',
  })

  const leftWall = Bodies.rectangle(x - WIDTH / 2, y - HEIGHT / 2, HEIGHT / 4, HEIGHT * 2, { isStatic: true })
  const rightWall = Bodies.rectangle(x + WIDTH / 2, y - HEIGHT / 2, HEIGHT / 4, HEIGHT * 2, { isStatic: true })

  const body = Composite.create({
    bodies: [
      title,
      floor,
      leftWall,
      rightWall,
    ]
  })

  base.addComponent( new BodyComponent(body) )

  return base
}
