import { Entity }             from 'ces'
import { Body }               from 'matter-js'
import BodyComponent          from '../components/Body.js'
import KeyboardInputComponent from '../components/KeyboardInput.js'
import ActionsComponent       from '../components/Actions.js'
import CameraTrackComponent   from '../components/CameraTrack.js'

const HEIGHT               = 25
const WIDTH                = HEIGHT / 1.5

export default function() {
  const player = new Entity()

  const body = Body.create({
    label: 'player',
    position: {
      x: 400,
      y: 300
    },
    vertices: [
      {x: 0, y: HEIGHT},
      {x: WIDTH / 2, y: 0},
      {x: WIDTH, y: HEIGHT}
    ],
  });

  player.addComponent( new BodyComponent(body) )
  player.addComponent( new KeyboardInputComponent() )
  player.addComponent( new ActionsComponent() )
  player.addComponent( new CameraTrackComponent() )

  return player
}
