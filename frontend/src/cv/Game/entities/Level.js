import { Entity }          from 'ces'
import { Bodies }          from 'matter-js'
import BodiesComponent     from '../components/Bodies.js'

const THICKNESS = 60
const WIDTH = 1000
const HEIGHT = 3000

export default function() {
  const level = new Entity()

  const boundsOptions = {
    isStatic: true,
    render: {
      fillStyle: '#222',
      strokeStyle: '#333',
    },
  }

  level.addComponent(new BodiesComponent([

    // upper boundry
    Bodies.rectangle(400, -2390, WIDTH, THICKNESS, boundsOptions),

    // lower boundry
    Bodies.rectangle(400, 610, WIDTH, THICKNESS, boundsOptions),

    // left boundry
    Bodies.rectangle(400 - WIDTH / 2, -890, THICKNESS, HEIGHT, boundsOptions),

    // right boundry
    Bodies.rectangle(400 + WIDTH / 2, -890, THICKNESS, HEIGHT, boundsOptions),

    // veda ground
    Bodies.rectangle(130, 320, 400, 30, boundsOptions),

    // lower blocks
    Bodies.rectangle(600, 200, 100, 100, boundsOptions),
    Bodies.rectangle(700, 300, 100, 100, boundsOptions),

    // outwide ground
    Bodies.rectangle(670, 0, 400, 30, boundsOptions),

    // first divider triple block
    Bodies.rectangle(130, -40, 100, 100, boundsOptions),
    Bodies.rectangle(230, -140, 100, 100, boundsOptions),
    Bodies.rectangle(330, -240, 100, 100, boundsOptions),

    // basetwo ground
    Bodies.rectangle(670, -400, 400, 30, boundsOptions),

    // aot ground
    Bodies.rectangle(420, -700, 700, 30, boundsOptions),
    Bodies.rectangle(400, -900, 15, 400, boundsOptions),

    // intrepid ground
    Bodies.rectangle(420, -900, 700, 30, boundsOptions),

    // apparel 21 ground
    Bodies.rectangle(0, -1300, 50, 50, boundsOptions),
    Bodies.rectangle(100, -1300, 50, 50, boundsOptions),
    Bodies.rectangle(300, -1300, 50, 50, boundsOptions),
    Bodies.rectangle(400, -1300, 200, 50, boundsOptions),
    Bodies.rectangle(500, -1300, 50, 50, boundsOptions),
    Bodies.rectangle(700, -1300, 50, 50, boundsOptions),
    Bodies.rectangle(800, -1300, 50, 50, boundsOptions),
    Bodies.rectangle(900, -1300, 50, 50, boundsOptions),

    // freelance and dragonfly ground
    Bodies.rectangle(300, -1500, 800, 10, boundsOptions),
    Bodies.rectangle(500, -1700, 800, 10, boundsOptions),

    // current ground
    Bodies.rectangle(400, -2100, 400, 20, boundsOptions),

  ]))

  return level
}
