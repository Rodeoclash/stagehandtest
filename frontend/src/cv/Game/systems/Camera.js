import { System }          from 'ces'
import {
  Vector,
  Bounds,
  Vertices,
}                          from 'matter-js'
import $                   from 'jquery'
import { getSize }         from '../shared/window.js'

const Camera = System.extend({

  init: function({engine, render}) {
    this.engine = engine
    this.render = render
  },

  addedToWorld: function (world) {
    this._super(world)
  },

  update: function(dt) {
    const {
      engine,
      render,
    } = this
    const size = getSize()
    const entities = this.world.getEntities('cameraTrack', 'body')

    entities.forEach(function (entity) {
      const entityBody = entity.getComponent('body')
      const {
        body: {
          position,
        },
      } = entityBody

      const deltaCentre = Vector.sub(Vector.sub(position, render.bounds.min), {x: size.width / 2, y: size.height / 2})
      const centreDist = Vector.magnitude(deltaCentre)

      if (centreDist > 10) {
        const direction = Vector.normalise(deltaCentre)
        const speed = Math.min(30, Math.pow(centreDist, 2) * 0.0010)
        const translate = Vector.mult(direction, speed)
        Bounds.translate(render.bounds, translate)
      }
    })
  }

})

export default Camera
