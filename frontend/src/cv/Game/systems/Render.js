import { System }             from 'ces'
import Matter                 from 'matter-js'
import _                      from 'lodash'
import $                      from 'jquery'
import { getSize }            from '../shared/window.js'

function resizeScene(event) {
    const {
      render: {
        canvas,
      },
      engine: {
        world,
      },
    } = this
  const size = getSize()

  canvas.width = size.width
  canvas.height = size.height
}

const debouncedResizeScene = _.debounce(resizeScene, 200)

const Render = System.extend({

  init: function({engine, render}) {
    this.engine = engine
    this.render = render
  },

  addedToWorld: function(world) {
    $(window).on('resize', debouncedResizeScene.bind(this))
    Matter.Render.run(this.render)
    this._super(world)
  },

  update: function(timestamp) {
    const {
      render: {
        canvas,
      },
    } = this

    Matter.Engine.update(this.engine)
  }
})

export default Render
