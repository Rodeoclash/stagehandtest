import { System } from 'ces'
import Matter from 'matter-js'
import { clamp } from 'lodash/fp'

const thrust = 0.00015

const topForcePoint = {
  x: 0,
  y: thrust,
}

const bottomForcePoint = {
  x: 0,
  y: thrust * -1,
}

const rightForcePoint = {
  x: thrust,
  y: 0,
}

const leftForcePoint = {
  x: thrust * -1,
  y: 0,
}

const Forces = System.extend({
  update: function(dt) {
    const entities = this.world.getEntities('actions', 'body')

    entities.forEach(function (entity) {
      const entityActions = entity.getComponent('actions')
      const entityBody = entity.getComponent('body')
      const { body } = entityBody
      const { action } = entityActions

      Matter.Body.setAngularVelocity(body, 0)
      Matter.Body.setAngle(body, clamp(-0.6, 0.6)(body.velocity.x * 0.15))

      if (action.thrustBottom) {
        Matter.Body.applyForce(body, body.position, bottomForcePoint)
      }

      if (action.thrustTop) {
        Matter.Body.applyForce(body, body.position, topForcePoint)
      }

      if (action.thrustLeft) {
        Matter.Body.applyForce(body, body.position, leftForcePoint)
      }

      if (action.thrustRight) {
        Matter.Body.applyForce(body, body.position, rightForcePoint)
      }

    })
  }
})

export default Forces
