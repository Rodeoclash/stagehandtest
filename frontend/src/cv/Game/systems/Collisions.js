import { System }    from 'ces'
import Matter        from 'matter-js'
import _             from 'lodash'

function getBaseFromPair(pair) {
  const {
    bodyA,
    bodyB,
  } = pair

  if (bodyA.label === 'base') {
    return bodyA
  }

  if (bodyB.label === 'base') {
    return bodyB
  }
}

function getBaseFromPairs(pairs) {
  for (let i = 0; i < pairs.length; i++) {
    const pair = pairs[i]
    const maybeBase = getBaseFromPair(pair)
    if (maybeBase) return maybeBase
  }
}

function setBaseState(base, active, ui) {
  ui.setBaseState(base.uiId, active)
}

const debouncedSetBaseActive = _.debounce(setBaseState, 250)

const Collision = System.extend({

  init: function({engine, ui}) {
    this.engine = engine
    this.ui = ui
  },

  addedToWorld: function(world) {
    Matter.Events.on(this.engine, 'collisionStart', event => {
      const maybeBase = getBaseFromPairs(event.pairs)
      if (maybeBase) debouncedSetBaseActive(maybeBase, true, this.ui)
    })

    Matter.Events.on(this.engine, 'collisionEnd', event => {
      const maybeBase = getBaseFromPairs(event.pairs)
      if (maybeBase) debouncedSetBaseActive(maybeBase, false, this.ui)
    })

    this._super(world)
  },

  update: function(timestamp) {
    // noop
  }

})

export default Collision
