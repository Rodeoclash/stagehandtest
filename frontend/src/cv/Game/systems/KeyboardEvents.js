import { System } from 'ces'
import $          from 'jquery'

let keysPressed = {}

const KeyboardEvents = System.extend({
  addedToWorld: function (world) {
    const body = $('body')

    body.on('keydown', function (event) {
      event.preventDefault()
      const { keyCode } = event
      keysPressed[keyCode] = true
    })

    body.on('keyup', function (event) {
      event.preventDefault()
      const { keyCode } = event
      keysPressed[keyCode] = false
    })

    this._super(world)
  },
  update: function(dt) {
    const entities = this.world.getEntities('keyboardInput')

    entities.forEach(function (entity) {
      let entityKeyboardInput = entity.getComponent('keyboardInput')
      entityKeyboardInput.keysPressed = keysPressed
    })
  }
})

export default KeyboardEvents
