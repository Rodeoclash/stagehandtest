import { System } from 'ces'
import $          from 'jquery'

let keysPressed = {}

const Control = System.extend({
  update: function(dt) {
    const entities = this.world.getEntities('keyboardInput', 'actions')

    /**
     * Map keyboard input to player actions
     */
    entities.forEach(function (entity) {
      const entityKeyboardInput = entity.getComponent('keyboardInput')
      let entityActions = entity.getComponent('actions')
      const { keysPressed } = entityKeyboardInput

      entityActions.action.thrustTop = keysPressed['40'] ? true : false // up arrow
      entityActions.action.thrustLeft = keysPressed['37'] ? true : false // left arrow
      entityActions.action.thrustBottom = keysPressed['38'] ? true : false // up arrow
      entityActions.action.thrustRight = keysPressed['39'] ? true : false // right arrow
    })
  }
})

export default Control
