import { map, camelCase } from 'lodash/fp'

const data = [
  {
    label: 'Home',
  },
  {
    about: `Factor 5 produces a product "Courseloop" for managing courses at universities. My role here as lead developer is to lead technology decisions and mentor junior developers.\n\nCourseloop is primarly a React/Relay frontend consuming the Service Now table API.\n\nInteresting challenges involving many dependent AJAX requests were solved in this product.`,
    label: 'Factor 5',
    links: [
      {
        name: 'Homepage',
        url: 'https://factor5.com.au/',
      },
    ],
    period: ['2016', 'Current'],
    technologies: [
      'ES6',
      'EsLint',
      'Flow',
      'Jest',
      'Lost Grid',
      'Fantasy Land Monads',
      'Ramda',
      'React',
      'Relay',
      'Service Now API',
      'Webpack 2',
    ],
    role: 'Lead Developer',
  },
  {
    about: `I worked on three different products in my time at Versent. The last, Stax, was the most interesting and involved consuming and presenting 100s GB of data from the Amazon AWS API.\n\nI worked closely with designers to ensure a set of reusable components for the application and accurate presentation of data in various chart formats / dashboards.`,
    label: 'Versent',
    links: [
      {
        name: 'Stax',
        url: 'https://stax.io/',
      },
      {
        name: 'Versent',
        url: 'https://versent.com.au',
      },
    ],
    period: ['2015', '2016'],
    technologies: [
      'ES6',
      'EsLint',
      'Flow',
      'Lodash',
      'Lost Grid',
			'Plotly.JS',
      'Postgres',
      'Rails',
      'React',
      'REST',
      'Relay',
      'Ruby',
      'Webpack 2',
    ],
    role: 'Senior developer',
  },
  {
    about: `Crosstivity was a startup building a workflow management tool. I worked closely with the owner/designer to create a frontend that would integrate with a Rails backend.\n\nThis product had some interesting visualisations and UX regarding finite state machines as part of the workflow designer.`,
    label: 'Crosstivity',
    links: [],
    period: ['2013', '2015'],
    technologies: [
			'JSPlumb',
      'Angular',
      'Crossfilter',
      'Gulp',
      'REST',
      'Rails',
      'Ruby',
      'TypeScript',
    ],
    role: 'Senior developer',
  },
  {
    about: 'Dragonfly was my personal startup, built with a friend of mine. We were accepted into a local incubator and after 3 months of intense development, showcased our work in San Francisco and New York.\n\nThe product connected local freelancers with local developers for short term design/development work, however was unprofitable.',
    label: 'Dragonfly',
    links: [],
    period: ['2012', '2013'],
    technologies: [
      'Angular beta',
      'Grunt',
      'JavaScript',
      'Rails',
      'Ruby',
    ],
    role: 'CTO',
  },
  {
    about: 'Deloitte Digital is one of Melbourne\`s premier design agencies. I spent a year here working on complex single page applications, including extensive work with Google Maps.\n\nAs well as working on client projects, I introduced the use of tool called "Middleman" to improve the speed of front end development being done.',
    label: 'Delottie Digital',
    links: [
      {
        name: 'Homepage',
        url: 'http://deloittedigital.com',
      },
    ],
    period: ['2011', '2012'],
    technologies: [
      'JavaScript',
      'MiddleMan',
      'Ruby',
    ],
    role: 'Senior developer',
  },
  {
    about: 'Igloo was a Melbourne design agency that is now sadly defunct. I worked here mainly on some interesting clients, including many theme parks!',
    label: 'Igloo',
    links: [],
    period: ['2009', '2011'],
    technologies: [
      'HTML',
      'JavaScript',
    ],
    role: 'Developer',
  },
  {
    about: 'Over the years I have worked on and off as a freelancer. The projects have been very varied but usually involve complex single page applications.\n\nI have been involved at all stages in projects when freelancing, from initial concepts through to last minute rush fixes.',
    label: 'Freelance',
    links: ['https://www.richardson.co.nz'],
    period: ['2005', '2017'],
    technologies: [
      'Clojure',
      'ClojureScript',
      'ES6/7',
      'Elixir',
      'Go',
      'Rails',
      'React',
      'Relay',
      'Ruby',
    ],
    role: 'Owner',
  },
  {
    about: 'Intrepid travel is a multinational travel company that provides local tours in destinations all over the earth. I was involved in a year long, complete redesign and rebuild of the homepage and booking system.',
    label: 'Intrepid Travel',
    links: ['http://www.intrepidtravel.com'],
    period: ['2006', '2007'],
    technologies: [
      'HTML',
      'JavaScript',
    ],
    role: 'Frontend developer',
  },
  {
    about: 'Australia Outback Travel is an inbound travel services company. It was a fairly standard job building various applications to support the business. Notable only as the first job I had once I arrived in Australia from New Zealand.',
    label: 'Australia Outback Travel',
    links: ['http://www.aot.com.au'],
    period: ['2005', '2006'],
    technologies: [
      'HTML',
      'JavaScript',
    ],
    role: 'Developer',
  },
]

export default map((datum) => {
  return {
    ...datum,
    id: camelCase(datum.label),
  }
})(data)
