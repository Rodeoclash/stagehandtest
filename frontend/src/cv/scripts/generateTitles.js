import fs from 'fs-extra'
import gm from 'gm'
import path from 'path'
import { flow, map } from 'lodash/fp'
import data from '../data.js'

const im = gm.subClass({ imageMagick: true });

const outputPath = path.resolve(__dirname, '../Game/assets/')

const writeTitle = (entry) => {
  return new Promise((resolve, reject) => {

    const year = entry.period && entry.period[0] ? `(${entry.period[0]})` : ''

    im(800, 600)
      .background('none')
      .font('Press-Start-2P-Regular', 8)
      .fill('#fff')
      .out(`caption: ${entry.label} ${year}`)
      .trim()
      .write(`${outputPath}/title_${entry.id}.png`, function (err) {
        err ? Promise.reject(err) : Promise.resolve()
      })
  })
}

const writeTitles = flow(
  map(writeTitle),
)

fs.emptyDir(outputPath, err => {
  Promise.all(writeTitles(data)).then(() => {
    console.log('All done')
  })
})
