{
  "access_token": "{{STAGEHAND_ACCESS_TOKEN}}",
  "command": "./stagehand_startup.sh",
  "feedback": {
    "key": "mykey",
    "name": "My Cool Preview",
    "description": "Some details about it"
  },
  "ports": [
    {
      "name": "Rancher Hello World",
      "description": "The first hello world",
      "value": 8001
    },
    {
      "name": "Crcheck Hello World",
      "description": "And the second hello world example",
      "value": 8000
    }
  ],
  "repo_slug": "{{BITBUCKET_REPO_SLUG}}",
  "sha": "{{BITBUCKET_COMMIT}}",
  "username": "Rodeoclash",
  "version": 1
}
